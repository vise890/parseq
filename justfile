alias clojurefmt := fmt
alias antq := upgrade-deps

# run tests
test:
  clojure -M:test

# reformat codebase
fmt:
  clojure -M:cljfmt
  prettier --write README.md

# upgrade project dependencies
upgrade-deps:
  clojure -T:antq

# lint the codebase
clj-kondo:
  clj-kondo --lint src test

eastwood:
  clojure -M:eastwood

lint: eastwood clj-kondo

docs:
  clojure -X:codox

deploy:
  clojure -T:build jar
  clojure -T:build deploy

clean:
  clojure -T:build clean

# run all tasks (e.g., in preparation for a commit)
all: upgrade-deps test fmt lint
