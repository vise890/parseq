(ns parseq.test-utils
  (:require [clojure.test :as t]
            [parseq.utils :as pu]))

(defmacro deftest-all-complete-parses
  [tname & body]
  (let [parses (remove nil? body)]
    `(t/deftest ~(symbol (str tname "-test"))
       ~@(for [p parses]
           `(t/is (pu/complete-success? ~p))))))

