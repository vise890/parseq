(ns parseq.json-test
  "Resources
    - mostly taken from: https://parsy.readthedocs.io/en/latest/howto/other_examples.html#json-parser
    - JSON grammar: https://www.json.org/json-en.html"
  (:require
   [clojure.string :as str]
   [clojure.test :refer [deftest is testing]]
   [parseq.combinators :as pc]
   [parseq.parsers :as pp]
   [parseq.test-utils :refer [deftest-all-complete-parses]]
   [parseq.utils :as pu]))

(comment
  ;; FIXME: fix cider reloading issues (alias already present in namespace)

  (do
    (ns-unalias *ns* 'sut)
    (ns-unalias *ns* 'pp)
    (ns-unalias *ns* 'pu)
    (ns-unalias *ns* 'pc))

  nil)

(def whitespace
  (pc/skip* (pp/one-satisfying #(Character/isWhitespace %))))
(deftest-all-complete-parses whitespace
  (pu/parse whitespace "  \t")
  nil)

(defn lexeme
  "Parses `p`, surrounded by `whitespace`."
  [p]
  (pc/surrounded-by whitespace p))
(defn lexeme-chr
  "Parses with `(pp/one= chr)`, surrounded by `whitespace`."
  [chr]
  (lexeme (pp/one= chr)))
(defn string
  "Parses a sequence of chars `s`. Returns `s`."
  [s]
  (->> (apply pc/cat (map pp/one= s))
       (pc/fmap (constantly s))))
(defn lexeme-str
  "Parses a sequence of chars `s`. Returns `ret`."
  [s ret]
  (->> (lexeme (string s))
       (pc/fmap (constantly ret))))

(defn repeated
  "Parses `p`, `n` times"
  [n p]
  (apply pc/cat (repeat n p)))
(defn resulting
  "Parses `p`, discards the result and returns `x` instead."
  [x p]
  (pc/fmap (constantly x) p))
(defn enclosed-by
  "Returns a parser that parses with `(cat left-p p right-p)`. Returns the result of `p`."
  [left-p right-p p]
  (->> (pc/cat left-p
               p
               right-p)
       (pc/fmap second)))

(def left-brace (lexeme-chr \{))
(def right-brace (lexeme-chr \}))
(def left-bracket (lexeme-chr \[))
(def right-bracket (lexeme-chr \]))
(def colon (lexeme-chr \:))
(def comma (lexeme-chr \,))

(def true- (lexeme-str "true" true))
(def false- (lexeme-str "false" false))
(def null- (lexeme-str "null" nil))
(deftest-all-complete-parses true-false-null
  (pu/parse true- " true ")
  (pu/parse false- "false ")
  (pu/parse null- "null")
  nil)

(def digit
  (pp/one-satisfying #(Character/isDigit %)))
(def positive-digit
  (pp/one-satisfying (->> (range 1 10) (apply str) (set))))
(def number
  (->> (pc/cat
        ;; optional sign
        (pc/one? (pp/one= \-))
        ;; integral part
        (pc/alt (pp/one= \0)
                (pc/cat positive-digit (pc/many* digit)))
        ;; optional fractional part
        (pc/one? (pc/cat (pp/one= \.)
                         (pc/many+ digit)))
        ;; optional scientific notation part
        (pc/one? (pc/cat (pc/alt (pp/one= \e) (pp/one= \E))
                         (pc/one? (pc/alt (pp/one= \+) (pp/one= \-)))
                         (pc/many+ digit))))
       (pc/fmap (comp bigdec str/join flatten))))
(deftest-all-complete-parses numbers

  (pu/parse digit "1")

  (pu/parse number "123")
  (pu/parse number "123.40")
  (pu/parse number "123.40e1")
  (pu/parse number "-123.40e13")
  (pu/parse number "0.40E2")
  (pu/parse number "-0.40E2")

  nil)

(def string-part
  (pc/many+ (pp/one-not-satisfying #{\\ \"})))
(defn hex-digit?
  [ch]
  (re-matches #"[0-9a-fA-F]" (str ch)))
(def hex-digit
  (pp/one-satisfying hex-digit?))
(def unicode-slug
  ;; unicode; e.g., \u24D2
  (pc/cat>> (pp/one= \u)
            (->> (apply pc/cat (repeat 4 hex-digit))
                 (pc/fmap (comp char #(BigInteger. % 16) str/join)))))
(def string-escaped
  (pc/cat>> (pp/one= \\)
            (pc/alt (pp/one= \")
                    (pp/one= \\)
                    (pp/one= \/)
                    (->> (pp/one= \b) (resulting \backspace))
                    (->> (pp/one= \f) (resulting \formfeed))
                    (->> (pp/one= \n) (resulting \newline))
                    (->> (pp/one= \r) (resulting \return))
                    (->> (pp/one= \t) (resulting \tab))
                    unicode-slug)))
(def quoted-string
  (pc/surrounded-by (pp/one= \")
                    (->> (pc/many* (pc/alt string-part string-escaped))
                         (pc/fmap (comp str/join flatten)))))
(deftest-all-complete-parses strings

  (pu/parse string-escaped "\\\"")
  (pu/parse string-escaped "\\\\")
  (pu/parse string-escaped "\\/")
  (pu/parse string-escaped "\\b")
  (pu/parse string-escaped "\\f")
  (pu/parse string-escaped "\\n")
  (pu/parse string-escaped "\\r")
  (pu/parse string-escaped "\\t")

  (pu/parse hex-digit "2")
  (pu/parse unicode-slug "u24D2")
  (pu/parse string-escaped "\\u24D2")

  (pu/parse quoted-string "\"foo\"")
  (pu/parse quoted-string "\"foo \"")
  (pu/parse quoted-string "\"foo \\t \\\" \"")
  (pu/parse quoted-string "\"foo \\t \\b \\n \\r \\\\ \\\" \\/ \\u2605\"")

  nil)

(declare json-value)
(def json-object-kv
  (pc/cat (pc/cat<< quoted-string colon)
          #'json-value))
(def json-object
  (enclosed-by left-brace
               right-brace
               (->> (pc/sep-by comma json-object-kv)
                    (pc/fmap (partial into {})))))
(def json-array
  (->> (enclosed-by left-bracket
                    right-bracket
                    (pc/sep-by comma #'json-value))
       (pc/fmap (partial into []))))
(def json-value
  (pc/alt quoted-string true- false- null- number json-object json-array))
(deftest-all-complete-parses json-value

  (pu/parse json-value "\"1\"")
  (pu/parse json-value "1")
  (pu/parse json-value "true")
  (pu/parse json-value "false")
  (pu/parse json-value "null")

  (pu/parse json-value "{}")
  (pu/parse json-value "[]")

  (pu/parse json-value "[1, 2]")
  (pu/parse json-value "[\"foo\", 1, [1 ,2]]")
  (pu/parse json-value "[true, false, \"foo\", null]")

  (pu/parse json-object-kv "\"foo\": 1")
  (pu/parse json-object "{\"foo\": 1}")
  (pu/parse json-object "{\"foo\"   : 1}")
  (pu/parse json-object "{\"foo\": 1, \"bar\": \"baz\"}")
  (pu/parse json-value "{\"foo\": \"bar\", \"baz\": [1, 2, null]}")

  nil)

(def lexeme-json-value
  "A `surrounded-by` `whitespace` `json-value`"
  (lexeme json-value))
(deftest json-value-from-parsy
  (testing "https://parsy.readthedocs.io/en/latest/howto/other_examples.html#json-parser"
    (let [got (pu/parse lexeme-json-value
                        " {
                            \"number\": 42,
                            \"string\": \"foo\",
                            \"array\": [1, 2, 3],
                            \"string escapes\": \"\n \u24D2\",
                            \"nested\": {\"x\": \"y\", \"z\": [1, 2]},
                            \"other\": [true, false, null]
                        }")]
      (is (pu/complete-success? got))
      (is (= {"number" 42M,
              "string" "foo",
              "array" [1M 2M 3M],
              "string escapes" "\n ⓒ",
              "nested" {"x" "y", "z" [1M 2M]},
              "other" [true false nil]}
             (pu/value got))))))

