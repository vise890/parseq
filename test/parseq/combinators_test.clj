(ns parseq.combinators-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [parseq.combinators :as sut]
   [parseq.parsers :as pp]
   [parseq.utils :as pu]))

(comment
  ;; FIXME: fix cider reloading issues (alias already present in namespace)

  (do
    (ns-unalias *ns* 'sut)
    (ns-unalias *ns* 'pp)
    (ns-unalias *ns* 'pu))

  nil)

(deftest alt-test
  (testing "lets you specify alternatives"
    (is (= [:hi [:hola]]
           (pu/parse (sut/alt pp/fail pp/one) [:hi :hola]))))

  (testing "works at the end of input"
    (is (= [:hi []]
           (pu/parse (sut/alt pp/fail pp/one) [:hi]))))

  (testing "fails when no parser matches"
    (is (pu/failure? (pu/parse (sut/alt pp/fail)
                               [:beep])))

    (testing "keeps track of failures"
      (let [res (pu/parse (sut/alt pp/fail
                                   pp/fail)
                          [:hi])
            pfs (:failures res)]
        (is (pu/failure? res))
        (is (every? pu/failure? pfs))
        (is (= 2 (count pfs)))))))

(deftest unordered-pair-test
  (let [p (sut/unordered-pair (pp/one= :a)
                              (pp/one= :b))]
    (is (= [[:a :b] []] (pu/parse p [:a :b])))
    (is (= [[:a :b] []] (pu/parse p [:b :a])))))

(deftest cat-test
  (testing "applies parsers in succession"
    (is (= [[1 2] [:a :a]]
           (pu/parse (sut/cat (pp/one= 1) (pp/one= 2))
                     [1 2 :a :a]))))

  (testing "fails when not all parsers match"
    (is (pu/failure? (pu/parse (sut/cat (pp/one= 1)
                                        (pp/one= 2))
                               [1 3])))))

(deftest cat>>-test
  (testing "applies parsers in succession and keeps last result only"
    (is (= [3 [:a :a]]
           (pu/parse (sut/cat>> (pp/one= 1) (pp/one= 2) (pp/one= 3))
                     [1 2 3 :a :a]))))

  (testing "fails when not all parsers match"
    (is (pu/failure? (pu/parse (sut/cat>> (pp/one= 1)
                                          (pp/one= 2))
                               [1 3])))))

(deftest cat<<-test
  (testing "applies parsers in succession and keeps first result only"
    (is (= [1 [:a :a]]
           (pu/parse (sut/cat<< (pp/one= 1) (pp/one= 2) (pp/one= 3))
                     [1 2 3 :a :a]))))

  (testing "fails when not all parsers match"
    (is (pu/failure? (pu/parse (sut/cat<< (pp/one= 1)
                                          (pp/one= 2))
                               [1 3])))))

(deftest one?-test
  (testing "applies the parser"
    (is (= [:a [:b :c]]
           (pu/parse (sut/one? pp/one)
                     [:a :b :c]))))

  (testing "does not fail if the parser fails"
    (is (= [nil [:a :b :c]]
           ;; NOTE `one?` is a.k.a. `optional`
           (pu/parse (sut/optional pp/fail)
                     [:a :b :c])))))

(deftest many*-test
  (testing "takes 0 or more repetitions"
    (is (= [[:a :b :c] []]
           (pu/parse (sut/many* pp/one) [:a :b :c])))
    (is (= [[] [:a :b :c]]
           (pu/parse (sut/many* pp/fail) [:a :b :c]))))

  (testing "can be combined with sut/alt"
    (is (= [[:a :b :a] [:c :c]]
           (pu/parse (sut/many* (sut/alt (pp/one= :a)
                                         (pp/one= :b)))
                     [:a :b :a :c :c]))))

  (testing "stops parsing correctly"
    (is (= [[1 1 1] [:a :a :a]]
           (pu/parse (sut/many* (pp/one= 1)) [1 1 1 :a :a :a]))))

  (testing "returns [] when input is nil"
    (is (= [[] []]
           (pu/parse (sut/many* pp/one) [])))
    (is (= [[] nil]
           (pu/parse (sut/many* pp/one) nil)))))

(deftest many+-test
  (testing "takes multiple elements"
    (is (= [[:a :a] [:b :c]]
           (pu/parse (sut/many+ (pp/one= :a))
                     [:a :a :b :c]))))

  (testing "requires at least one match"
    (is (pu/failure? (pu/parse (sut/many+ (pp/one= :a))
                               [:f :o :o]))))

  (testing "maintains order"
    (is (= [[1 2] [:a]]
           (pu/parse (sut/many+ (pp/one-satisfying integer?))
                     [1 2 :a])))
    (is (= [["a" "b"] [:c]]
           (pu/parse (sut/many+ (pp/one-satisfying string?))
                     ["a" "b" :c])))))

(deftest skip*-test
  (testing "returns nil"
    (is (= [nil [:b]]
           (pu/parse (sut/skip* (pp/one= :a)) [:a :a :b]))))

  (testing "parses 0 repetitions"
    (is (= [nil [:a :b]]
           (pu/parse (sut/skip* (pp/one= :b)) [:a :b])))))

(deftest skip+-test
  (testing "returns nil"
    (is (= [nil [:b]]
           (pu/parse (sut/skip+ (pp/one= :a)) [:a :a :b]))))

  (testing "requires at least one match"
    (is (pu/failure? (pu/parse (sut/skip+ (pp/one= :b))
                               [:a :b])))))

(deftest skip1-test
  (testing "returns nil"
    (is (= [nil [:a :b]]
           (pu/parse (sut/skip1 (pp/one= :a)) [:a :a :b]))))

  (testing "requires at least one match"
    (is (pu/failure? (pu/parse (sut/skip1 (pp/one= :b))
                               [:a :b])))))

(deftest map-merge-test
  (let [p   (sut/map-merge (sut/fmap (fn [one] {:one one})
                                     (pp/one= 1))
                           (sut/fmap (fn [two] {:two two})
                                     (pp/one= 2))
                           (sut/fmap (fn [three] {:three three})
                                     (pp/one= 3)))
        act (pu/parse p [1 2 3 :nope])]
    (is (= [{:one   1
             :two   2
             :three 3} [:nope]]
           act)))

  (testing "more parsers than groups should fail"
    (is (pu/failure? (pu/parse (sut/map-merge pp/one pp/one)
                               [[1 2]]))))

  (testing "parses nil input if no parsers specified"
    (is (= [nil nil]
           (pu/parse (sut/map-merge) nil))))

  (testing "no parsers yields nil map and consumes no input"
    (is (= [nil [[1 2]]]
           (pu/parse (sut/map-merge) [[1 2]])))))

(deftest into-map-test
  (let [p   (sut/into-map (sut/fmap (fn [one] [:one one])
                                    (pp/one= 1))
                          (sut/fmap (fn [two] [:two two])
                                    (pp/one= 2))
                          (sut/fmap (fn [three] [:three three])
                                    (pp/one= 3)))
        act (pu/parse p [1 2 3 :nope])]
    (is (= [{:one   1
             :two   2
             :three 3} [:nope]]
           act)))

  (testing "parses nil input if no parsers specified"
    (is (= [{} nil]
           (pu/parse (sut/into-map) nil))))

  (testing "no parsers yields an empty map and consumes no input"
    (is (= [{} [[1 2]]]
           (pu/parse (sut/into-map) [[1 2]])))))

(deftest named-test
  (let [p   (sut/into-map (sut/named :one (pp/one= 1))
                          (sut/named :two (pp/one= 2))
                          (sut/named :three (pp/one= 3)))
        act (pu/parse p [1 2 3 :nope])]
    (is (= [{:one   1
             :two   2
             :three 3} [:nope]]
           act))))

(deftest altn-test
  (let [p   (sut/altn :one (pp/one= 1)
                      :two (pp/one= 2)
                      :three (pp/one= 3))
        act (pu/parse (sut/many* p) [1 2 3 :nope])]
    (is (= [[[:one   1]
             [:two   2]
             [:three 3]] [:nope]]
           act))))

(deftest catn-test
  (let [p   (sut/catn :one (pp/one= 1)
                      :two (pp/one= 2)
                      :three (pp/one= 3))
        act (pu/parse p [1 2 3 :nope])]
    (is (= [[[:one   1]
             [:two   2]
             [:three 3]] [:nope]]
           act))))

(deftest peek-test
  (testing "consumes no input"
    (is (= [1 [1 [2 3]]]
           (pu/parse (sut/peek pp/one) [1 [2 3]]))))

  (testing "fails when there's no more input left"
    (is (pu/failure?
         (pu/parse (sut/peek pp/one) [])))))

(deftest bind-test
  (is (= [2 [3]]
         (pu/parse (sut/bind pp/one
                             (fn [_2] pp/one))
                   [1 2 3])))

  (is (pu/failure? (pu/parse (sut/bind pp/fail
                                       (fn [_] pp/one))
                             [[]]))))

(deftest fmap-test
  (is (= ["1" [:fin]]
         (pu/parse (sut/fmap str pp/one) [1 :fin]))))

(deftest unordered-test
  (let [p (sut/many* (sut/alt (pp/one= :a)
                              (pp/one= :b)))]
    (is (= [[:a :b :a] [:c]]
           (pu/parse p [:a :b :a :c]))))

  (testing "A more complex example"
    (let [p (sut/fmap #(apply (partial merge-with concat) %)
                      (sut/many+ (sut/alt (sut/fmap (fn [one] {:one one})
                                                    (sut/many+ (pp/one= 1)))
                                          (sut/fmap (fn [two] {:two two})
                                                    (sut/many+ (pp/one= 2)))
                                          (sut/fmap (fn [three] {:three three})
                                                    (sut/many+ (pp/one= 3))))))]
      (is (= [{:one   [1 1 1]
               :two   [2 2]
               :three [3 3]} [:nope]]
             (pu/parse p [1 1 3 2 2 1 3 :nope]))))))

(deftest surrounded-by-test
  (is (= [0 [4]]
         (pu/parse (sut/surrounded-by (pp/one= 1)
                                      (pp/one= 0))
                   [1 0 1 4]))))

(deftest enum-test
  (is (= [[:bar :foo] [1 4]]
         (pu/parse (sut/many* (sut/enum {:foo "foo", :bar 42}))
                   [42 "foo" 1 4]))))

(deftest sep-by-test
  (is (= [[0 0] [1]]
         (pu/parse (sut/sep-by (pp/one= 1)
                               (pp/one= 0))
                   [0 1 0 1])))
  (testing "nil when no parses"
    (is (= [nil "3,4"]
           (pu/parse (sut/sep-by (pp/one= \,)
                                 (pp/one= \1))
                     "3,4"))))
  (testing "trailing separator"
    (is (= [(seq "11") (seq ",4")]
           (pu/parse (sut/sep-by (pp/one= \,)
                                 (pp/one= \1))
                     "1,1,4")))))

