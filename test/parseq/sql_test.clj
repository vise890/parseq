(ns parseq.sql-test
  "Inspiration taken from https://parsy.readthedocs.io/en/latest/howto/other_examples.html#sql-select-statement-parser"
  (:require
   [clojure.string :as str]
   [clojure.test :refer [deftest is testing]]
   [parseq.combinators :as pc]
   [parseq.parsers :as pp]
   [parseq.test-utils :refer [deftest-all-complete-parses]]
   [parseq.utils :as pu]))

(comment
  ;; FIXME: fix cider reloading issues (alias already present in namespace)

  (do
    (ns-unalias *ns* 'sut)
    (ns-unalias *ns* 'pp)
    (ns-unalias *ns* 'pu)
    (ns-unalias *ns* 'pc))

  nil)

(def number (->> (pp/one-re #"-?\d+")
                 (pc/fmap parse-long)))
(def string (->> (pp/one-re #"[^']*")
                 (pc/surrounded-by (pp/one= "'"))))
(def identifier (pp/one-re #"\w[\w0-9_]*"))
(deftest-all-complete-parses number|string|identifier
  (pu/parse number ["42"])
  (pu/parse string ["'" "foo" "'"])
  (pu/parse identifier ["foo"])
  nil)

(def column (pc/altn :number number
                     :field identifier
                     :string string))
(def columns (pc/sep-by (pp/one= ",") column))
(deftest-all-complete-parses column
  (pu/parse (pc/many+ column)
            ["'" "string" "'" "identifier" "42"])
  nil)

(def operator (pc/enum {:eq "=", :lt "<", :gt ">", :lte "<=", :gte ">="}))
(deftest-all-complete-parses operator
  (pu/parse (pc/many+ operator)
            ["=" "<" ">" "<=" ">="])
  nil)

(def comparison (pc/cat column operator column))
(deftest-all-complete-parses comparison
  (pu/parse comparison ["1" "=" "1"])
  nil)

(def SELECT (pp/one= "SELECT"))
(def FROM (pp/one= "FROM"))
(def WHERE (pp/one= "WHERE"))
(def AND (pp/one= "AND"))
(def table identifier)
(def select-statement
  (pc/cat<< (pc/catn :columns (pc/cat>> SELECT columns)
                     :table (pc/cat>> FROM table)
                     :where (pc/cat<< (pc/optional (pc/cat>> WHERE (pc/sep-by AND comparison)))))
            (pp/one= ";")))

(defn tokenize
  [s]
  (-> s
      (str/replace #"([',;=])" " $1 ")
      (str/split #"\s+")))

(deftest-all-complete-parses select-statement
  (pu/parse select-statement
            (-> "SELECT lowercase_identifier ,
                        Uppercaze_identifier,
                        -123, 42,
                        'Hello'
                 FROM the_table
                 WHERE foo= 1 AND bar = 2 ;"
                tokenize))

  nil)

(deftest sql-test
  (testing "simplified select"
    (let [got (pu/parse select-statement
                        (tokenize "SELECT lowercase_identifier ,
                                          Uppercaze_identifier,
                                          -123, 42,
                                          'Hello'
                                  FROM the_table;"))]
      (is (pu/all-input-parsed? got))
      (is (= [[[:columns [[:field "lowercase_identifier"]
                          [:field "Uppercaze_identifier"]
                          [:number -123]
                          [:number 42]
                          [:string "Hello"]]]
               [:table "the_table"]
               [:where nil]]
              ()]
             got))))
  (testing "simplified select + where"
    (let [got (pu/parse select-statement
                        (tokenize "SELECT lowercase_identifier ,
                                          Uppercaze_identifier,
                                          -123, 42,
                                          'Hello'
                                  FROM the_table
                                  WHERE foo= 1;"))]
      (is (pu/all-input-parsed? got))
      (is (= [[[:columns [[:field "lowercase_identifier"]
                          [:field "Uppercaze_identifier"]
                          [:number -123]
                          [:number 42]
                          [:string "Hello"]]]
               [:table "the_table"]
               [:where [[[:field "foo"] :eq [:number 1]]]]]
              ()]
             got))))
  (testing "simplified select + where + and"
    (let [got (pu/parse select-statement
                        (tokenize "SELECT lowercase_identifier ,
                                          Uppercaze_identifier,
                                          -123, 42,
                                          'Hello'
                                  FROM the_table
                                  WHERE foo= 1 AND bar = 2;"))]
      (is (pu/all-input-parsed? got))
      (is (= [[[:columns [[:field "lowercase_identifier"]
                          [:field "Uppercaze_identifier"]
                          [:number -123]
                          [:number 42]
                          [:string "Hello"]]]
               [:table "the_table"]
               [:where [[[:field "foo"] :eq [:number 1]]
                        [[:field "bar"] :eq [:number 2]]]]]
              ()]
             got)))))

