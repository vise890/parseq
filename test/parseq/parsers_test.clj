(ns parseq.parsers-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [parseq.parsers :as sut]
   [parseq.utils :as pu]))

(deftest fail-test
  (testing "always fails"
    (is (pu/failure? (pu/parse sut/fail [1 2])))))

(deftest suceed-test
  (testing "always succeeds"
    (is (pu/success? (pu/parse sut/succeed [1 2]))))
  (testing "returns nil and does not consume any input"
    (is (= [nil [1 2]] (pu/parse sut/succeed [1 2])))))

(deftest one-test
  (testing "takes one element unconditionally"
    (is (= [:beep [:boop]]
           (pu/parse sut/one [:beep :boop]))))

  (testing "fails when no more input"
    (is (pu/failure? (pu/parse sut/one nil))))

  (testing "works on lists"
    (is (= [:beep [:boop]]
           (pu/parse sut/one '(:beep :boop))))))

(deftest one-satisfying-test
  (testing "takes elements that satisfy a predicate"
    (is (= [3 [2]]
           (pu/parse (sut/one-satisfying odd?) [3 2]))))

  (testing "fails when input does not match"
    (is (pu/failure? (pu/parse (sut/one-satisfying odd?) [2]))))

  (testing "fails when no more input"
    (is (pu/failure? (pu/parse (sut/one-satisfying odd?) []))))

  (testing "works on lists"
    (is (= [3 [2]]
           (pu/parse (sut/one-satisfying odd?) '(3 2))))))

(deftest one-not-satisfying-test
  (testing "takes elements that do not satisfy a predicate"
    (is (= [3 [2]]
           (pu/parse (sut/one-not-satisfying even?) [3 2]))))

  (testing "fails when input does not match"
    (is (pu/failure? (pu/parse (sut/one-not-satisfying even?) [2]))))

  (testing "fails when no more input"
    (is (pu/failure? (pu/parse (sut/one-not-satisfying even?) [])))))

(deftest one=-test
  (is (= [1 [2]]
         (pu/parse (sut/one= 1) [1 2])))

  (testing "fails when input does not match"
    (is (pu/failure? (pu/parse (sut/one= 1) [:a :b])))))

(deftest one-not=-test
  (is (= [1 [2]]
         (pu/parse (sut/one-not= :a) [1 2])))

  (testing "fails when input does not match"
    (is (pu/failure? (pu/parse (sut/one-not= :foo) [:foo :b])))))

(deftest one-re-test
  (is (= ["1" [2]]
         (pu/parse (sut/one-re #"\d") ["1" 2])))

  (testing "fails when input does not match"
    (is (pu/failure? (pu/parse (sut/one-re #"\w") ["123" :b]))))

  (testing "fails when input is not a string"
    (is (pu/failure? (pu/parse (sut/one-re #"\w") [:foo :b])))))
