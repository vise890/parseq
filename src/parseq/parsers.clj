(ns parseq.parsers
  "An eclectic bunch of parsers."
  (:require
   [parseq.utils :as pu]))

(defn ->fail
  "Builds a parser that always fails."
  ([msg] (fn [_input] (pu/->failure msg)))
  ([msg data] (fn [_input] (pu/->failure msg data))))

(def fail
  "A parser that always fails"
  (->fail "`fail` failed, it always does that..."))

(defn succeed
  "A parser that always succeeds, returning `nil`"
  [input]
  [nil input])

(defn one-satisfying
  "Builds a parser that takes an element that satisfies `predicate`."
  [predicate]
  (fn [input]
    (if (empty? input)
      (pu/->failure "`one-satisfying` failed: no more input"
                    {:predicate predicate})
      (if-not (predicate (first input))
        (pu/->failure "`one-satisfying` failed: `predicate` returned false for `v`"
                      {:input     input
                       :predicate predicate
                       :v         (first input)})
        [(first input) (rest input)]))))

(def one
  "A parser that takes (any) one element."
  (one-satisfying (constantly true)))

(defn one-not-satisfying
  "Builds a parser that takes an element that does not satisfy `predicate`."
  [predicate]
  (one-satisfying (complement predicate)))

(defn one=
  "Builds a parser that takes an element that equals `v`."
  [v]
  (one-satisfying (partial = v)))

(defn one-not=
  "Builds a parser that takes an element that equals `v`."
  [v]
  (one-not-satisfying (partial = v)))

(defn one-re
  "Builds a parser that takes an element that matches `re`."
  [re]
  (one-satisfying #(and (string? %) (re-matches re %))))
