(ns parseq.utils
  "Helpers for dealing with parsers and their results.")

(defn parse
  "Apply parser `p` to `input` (i.e. helper to run the parse)."
  [p input]
  (p input))

(defn success?
  "Checks that the result of a parse has succeeded."
  [parse-result]
  (and (vector? parse-result)
       (= 2 (count parse-result))))

(def failure?
  "Checks if the result of a parse has failed."
  (comp not success?))

(defmacro match-parse
  "Evaluates `parse-result` once. If it is a `success?`, destructures the parse result
  with `success?-binds` and evaluates `success?-body`. If it is a `failure?`,
  destructures the parse result with `failure?-binds` and evaluates `failure?-body`."
  [parse-result
   success?-binds success?-body
   failure?-bind  failure?-body]
  `(let [result# ~parse-result]
     (if (success? result#)
       (let [~success?-binds result#]
         ~success?-body)
       (let [~failure?-bind result#]
         ~failure?-body))))

(defn all-input-parsed?
  "Checks that the result of a parse has successfully parsed all the input."
  [parse-result]
  (and (success? parse-result)
       (empty? (second parse-result))))
(def complete-success?
  "Alias to `all-input-parsed?`"
  all-input-parsed?)

(defn value
  "Extracts the value from a successful parse result."
  [parse-result]
  (when (success? parse-result)
    (first parse-result)))

(defn ->failure
  "Constructs a parser Failure with optional data."
  ([msg] (->failure msg {}))
  ([msg data] (merge data
                     {:failure-msg msg})))
