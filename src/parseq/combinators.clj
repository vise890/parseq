(ns parseq.combinators
  "Combinators for `parseq.parsers` and similar.

  For simplicity the docstrings describe the parser that is returned by each combinator."
  (:refer-clojure :exclude [cat peek])
  (:require
   [parseq.parsers :as pp]
   [parseq.utils :as pu]))
;; TODO: move everything to `parseq.core` ns for ergonomics
;; TODO: rename peek to non-clashing name?

(defn return
  "A parser that does nothing and always succeeds. It returns the input
  unchanged and the supplied `v` as a result."
  [v]
  (fn [input] [v input]))

(defn bind
  "This is a monadic bind. A.k.a. `>>=`.

  Its type is:

  bind :: [Parser a, (a -> Parser b)] -> Parser b"
  [p f]
  (fn [input]
    (pu/match-parse (pu/parse p input)
                    [r rsin] (pu/parse (f r) rsin)
                    f f)))

(defn fmap
  "Applies function `f` to the result of `p`.

  Its type is:

  fmap :: [Parser a, (a -> b)] -> Parser b"
  [f p]
  (bind p #(return (f %))))

(defn named
  "Applies `p`, if it succeeds it returns the tuple `[n result]`"
  [n p]
  (fmap (fn [r] [n r]) p))

(defn alt
  "Tries `parsers` one after another. Returns the result from the first parser
  that succeeds. If all parsers fail, it fails."
  [& parsers]
  (fn [input]
    (loop [parsers  parsers
           failures []]
      (if-let [[p & ps] (not-empty parsers)]
        (pu/match-parse (pu/parse p input)
                        [r rsin] [r rsin]
                        f (recur ps (conj failures f)))
        (pu/->failure "`alt` had no more parsers"
                      {:failures failures})))))

(defn altn
  "Given `bindings` mappings of `name->parser`, associates the `name`s to the `parser`s with `pc/named`, then works as per `alt`."
  [& bindings]
  (apply alt (map (partial apply named)
                  (partition-all 2 bindings))))

(defn unordered-pair
  "Tries `p1` followed by `p2` or `p2` followed by `p1`. If either combination
  succeeds, always returns `[result1 result2]`. "
  [p1 p2]
  (alt (bind p1
             (fn [r1]
               (fmap (fn [r2] [r1 r2])
                     p2)))
       (bind p2
             (fn [r2]
               (fmap (fn [r1] [r1 r2])
                     p1)))))

(defn cat
  "Applies `parsers` one after another. If any parser fails, it fails."
  [& parsers]
  (fn [input]
    (loop [parsers parsers
           res     []
           input   input]
      (if-let [[p & ps] (not-empty parsers)]
        (pu/match-parse (pu/parse p input)
                        [r rsin] (recur ps (conj res r) rsin)
                        f f)
        [res input]))))

(defn cat>>
  "Applies `parsers` one after another as per `cat`. Returns the result from the last parser in `parsers` and discards the rest."
  [& parsers]
  (->> (apply cat parsers)
       (fmap last)))

(defn cat<<
  "Applies `parsers` one after another as per `cat`. Returns the result from the first parser in `parsers` and discards the rest."
  [& parsers]
  (->> (apply cat parsers)
       (fmap first)))

(defn catn
  "Given `bindings` mappings of `name->parser`, associates the `name`s to the `parser`s with `pc/named`, then works as per `cat`."
  [& bindings]
  (apply cat (map (partial apply named)
                  (partition-all 2 bindings))))

(defn one?
  "Optionally parses one `p`, returning its result, if
  found. If `p` doesn't match, returns an nil."
  [p]
  (fn [input]
    (pu/match-parse (pu/parse p input)
                    [r rsin] [r rsin]
                    _f [nil input])))
(def optional
  "Alias to `one?`"
  one?)

(defn many*
  "Parse `p` 0 or more times. Similar to `*` in regular expressions."
  [p]
  (fn [input]
    (loop [results    []
           rest-input input]
      (pu/match-parse (pu/parse p rest-input)
                      [r rsin] (recur (conj results r) rsin)
                      _ [results rest-input]))))

(defn many+
  "Parse `p` 1 or more times. Similar to `+` in regular expressions."
  [p]
  (bind p
        (fn [r]
          (fmap #(cons r %)
                (many* p)))))

(defn map-merge
  "Applies `parsers` in order and then merges their results into one map."
  [& parsers]
  (->> (apply cat parsers)
       (fmap (partial apply clojure.core/merge))))

(defn into-map
  "Applies `parsers` in order and then `into`s their results into one map. Useful for use with `named`."
  [& parsers]
  (->> (apply cat parsers)
       (fmap (partial into {}))))

(defn peek
  "Peeks with `p` (and fails if `p` fails). Does not consume any input."
  [p]
  (fn [input]
    (pu/match-parse (pu/parse p input)
                    [r _rsin] [r input]
                    f f)))

(defn skip*
  "Skips 0 or more `p`."
  [p]
  (fn [input]
    (loop [rest-input input]
      (pu/match-parse (pu/parse p rest-input)
                      [_ rsin] (recur rsin)
                      _ [nil rest-input]))))

(defn skip+
  "Skips 1 or more `p`."
  [p]
  (bind p (fn [_] (skip* p))))

(defn skip1
  "Skips one `p`."
  [p]
  (bind p (fn [_] (fn [input] [nil input]))))

(defn surrounded-by
  "Returns a parser that parses with `(cat surrounding-p p surrounding-p)`. Returns the result of `p`."
  [surrounding-p p]
  (->> (cat surrounding-p
            p
            surrounding-p)
       (fmap second)))

(defn enum
  "Given a map `bindings` of `name->literal-form`, it returns a parser that returns the `name` of the first `(pp/one= literal-form)` that succeeds."
  [bindings]
  (apply alt (for [[n lit] bindings]
               (fmap (constantly n) (pp/one= lit)))))

(defn sep-by
  "Returns a parser that repeatedly parses with `p` interposed with `separator-p` and discards the parses from `separator-p`"
  [separator-p p]
  (optional
   (->> (cat p
             (many* (cat>> separator-p p)))
        (fmap #(cons (first %) (second %))))))
